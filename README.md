# Steps to a repo initialization in the case you have the project on a pc and want to upload it, you can also find those commands in the repo you created

`git init` // initialize repository, you should only use this command the first time

`git add .` //used to add all your files that you want to push

`git commit -m "commit text"` //it prepares the files to be commited, also you should always put a something in the message related to your work, e.g. I finished the css but I still need to test the js

`git push -u origin master` //only usefull the first time you commit something to a repo

# Steps to push a commit to the remote after the repo is initialized

`git add .` 

`git commit -m "commit text"`

`git push`


# Push is used after "add" and "commit" and pull can be used anytime you don't have any changes on your local pc

`git push` // pushes your commit to the repository, you will use this most of the time you want to push something

`git pull` //pulls the changes from the repo. Most of the time, you want to use git pull instead of git fetch. Pull is pretty much git fetch + git merge combined, and it's waaaay easier to use pull instead of those 2


# Creating or moving to another branch

`git checkout -b branch_name` //creates a branch with that name

`git push -u origin branch_name` //only usefull the first time you commit something to a branch, after that you can just use git push (ofc with the add and commit before push)

`git checkout remote_branch_name` //this is a command you use when you want to go to another branch, note that you should save the branch you are working on using either add, commit and push or you can use stash but it's pretty wierd and I wouldn't recommend using it

`git branch -d branch_name` // deletes LOCALLY that branch

`git push origin --delete branch_name` // deletes REMOTLY that branch

# Merge process, let's say you want to merge your branch named "branch1" to another branch, let's say "branch2"
`git checkout branch2` //the branch that you want to merge your branch to

`git merge branch1` // merge your branch1 to the branch2

`git push` //I don't know for sure if you need to do this

# Usefull commands

`git reset --hard HEAD` //used when you want to go back to how your programm looks on the remote branch, for example if you made a change and you realised you don't need it, instead of deleteing the change, you can just use this command so you can be sure nothing will break

`git branch -a` //shows all the branches created, localy and on the remote 

`git diff` //usefull when you want to see the changes you made compared to the remote branch

`git log` // shows all the commits that have been done on the branch you are on, usefull when you want to go back to a commmit

`git checkout -b commit_hashcode` // after you use git log, you can go back to a commit by seeing the hashcode from that commit and creating a branch from it

`git clone http_link` // if the repository is already initialized and people want to get it, they should use this 


# GOOD PRACTICES

After you initialize the project, you pretty much never want to commit to the master branch (or any branch you want to be the latest stable version of the project)

Whenever someone wants to make another feature, bug fix, refactor or anything at all, they should always create a new branch

After someone is done with the thing they were working on, they should merge the branch that they worked on to master and after the merge, delete the branch that they worked on

The cool thing about git is that if you merge a branch to another branch, the history is also merged so you pretty much never lose your history, unless you use git reset --hard.

If you create a new branch, that branch will have all the code from the branch you were on when you used git checkout -b. Let's say you were on master, if you use git checkout -b branch1, branch1 will have all the code that master had at that time. You can also do this from branches other than master, like if you are on branch1 you can do git checkout -b branch2 and branch2 will pretty much be a copy of branch1.
